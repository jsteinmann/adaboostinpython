import numpy as np
from sklearn.tree import DecisionTreeClassifier

class AdaBoost:
    def __init__ (self, num_weak_models):
        self.num_weak_models = num_weak_models
        self.alphas = None
        self.classifiers = None
        
        
    def fit (self, X, Y):
        #initialize weights, alphas and classifiers
        weights = [1/len(X)] * len(X)
        alphas = []
        classifiers = []
        
        #get classifiers and alphas
        for round in range(self.num_weak_models):
            weak_model = DecisionTreeClassifier(criterion="entropy",max_depth=1)
            model = weak_model.fit(X,Y,sample_weight=np.array(weights))
            classifiers.append(model)
            
            predictions = np.array(model.predict(X))
            classified_right = predictions == Y
            misclassified = np.logical_not(classified_right)
            
            error = sum(weights * misclassified)
            
            alpha = np.log((1-error)/error)/2
            alphas.append(alpha)
            
            #update weights
            misclassified_ = 2*misclassified-1
            w = weights * np.exp(misclassified_ * alpha)
            weights = w/sum(w)
            
        #update alphas and classifiers in the object
        self.alphas = alphas
        self.classifiers = classifiers
        
        
    def predict (self, X):
        predictions = []
        
        for alpha,classifier in zip(self.alphas,self.classifiers):
            prediction = alpha*classifier.predict(X)
            predictions.append(prediction)
            
        result = np.sign(np.sum(np.array(predictions),axis=0))
        return result
    
    
    def score (self, X, Y):
        prediction = self.predict(X)
        y = np.array(Y)
        classified_right = prediction == y
        result = sum(classified_right)/len(y)
        return result