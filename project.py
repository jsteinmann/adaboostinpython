#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 13:26:58 2019

@author: Joshua Steinmann
@author: Stijn Boosman
"""

import pandas as pd
from adaboost import AdaBoost 

data = pd.read_csv("wdbc.data", header = None)
X = data.drop(columns=[0,1])
Y = data[1] == 'M'
Y = Y*2-1

model = AdaBoost(4)
model.fit(X,Y)
print(model.score(X,Y))

