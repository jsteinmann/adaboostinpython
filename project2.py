#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 13:26:58 2019

@author: Joshua Steinmann
@author: Stijn Boosman
"""

import pandas as pd
from adaboost import AdaBoost 
import matplotlib.pyplot as plt

data = pd.read_csv("wdbc.data", header = None)
X = data.drop(columns=[0,1])
Y = data[1] == 'M'
Y = Y*2-1

num_weak_learners = 34
lst = []
for i in range (1, num_weak_learners+1):    
    model = AdaBoost(i)
    model.fit(X,Y)
    score = model.score(X,Y)
    lst.append(score)
    print("Score:", str(score*100)+"%")

plt.plot (range(len(lst)), lst, '-b')
plt.xlabel("Number of models used")
plt.ylabel("Accuracy of each model")
plt.grid()
plt.show()